# SQLite

Tässä kansiossa on harjoitus joka havainnollistaa sqlite3
tietokantasovelluksen käyttöä tiliotteen hallinnointiin.

## infoa

SQL kielellä voidaan hallinnoida sqlite3 -tietokantaa.
Tietokanta tallentuu levylle ja siihen ei ole erillistä
pääsyn hallintaa (toimii kuten mikä tahansa tiedosto).
sqlite3 on yksi kevyimmistä ja pienimmistä sql-tietokanta
 -sovelluksista. Android-puhelimet esimerkiksi käyttävät
 sqlite3 tietokantaa sen keveyden vuoksi.

SQL tietokannat ovat kuin excel taulukoita ilman graafista
käyttöliittymää. Tällä periaatteella `.db` -päätteiset
tiedostot ovat kuten excel työkirjat ja tietokannan sisällä
olevat taulut ovat kuin excelin sisällä olevat välilehdet.

SQL kielen avulla poimintojen ja tiedon manipulointi on
suoraviivaista ja noudattaa hyvin pitkälle englannin kieltä.
Oppiminen on helppoa. Aloitetaan.

## Käyttöönotto ja asennus ensin

Luo itsellesi työkansio ja siirry sinne komentorivillä:

```powershell
cd .\Documents\tiliotteet\sqliteharjoitus\
```

Lataa sqlite3 sovellus osoitteesta: www.sqlite.org, 
[tässä suora linkki windows sqlite3een](https://www.sqlite.org/2020/sqlite-tools-win32-x86-3310100.zip)

Havainne gif:

![sqlite_lataus](img/sqlite_lataus.gif)

## Tietokannan alustus ja taulun tekeminen

Harjoitusta varten lataamme `csv` tiedoston tietokantaan 
`.import` -komennolla.

### CSV enkoodauksen vaihtaminen

Osuuspankki toimittaa CSV:nsä ANSI muodossa. SQLite3 tukee vain modernia
utf-8 merkistökoodausta. CSV:n ääkköset näkyvät tietokannassa väärin mikäli
tätä enkoodausta ei vaihda `ANSI -> UTF8`

Tämä onnistuu notepadilla. Avaa csv notepadiin ja tallenna tiedosto nimellä
uuteen tiedostoon, mutta vaihda encoding UTF8 -muotoon.

![enkoodauksen_vaihto](img/utf8.png)

### Alustus

Käynnistä sqlite3 komentoriviltä ja anna toisena argumenttina
uuden tietokantasi nimi. itse nimeän tietokantani `tietokanta.db`
muotoon.

```
PS C:\Users\Aki\Documents\tiliotteet\sqliteharjoitus> .\sqlite3.exe tietokanta.db
SQLite version 3.31.1 2020-01-27 19:55:54
Enter ".help" for usage hints.
sqlite>
```

Kaikki SQLite3 sisäiset komennot alkavat pisteellä. Kaikki komennot
saat listattua `.help` -komennolla.

SQLite luo uuden taulun automaattisesti käyttämällä .import -komentoa.

- Ensin täytyy vaihtaa mode csv-muotoon. `.mode csv`
- Tämän jälkeen vaihtaa vielä erotinmerkki pilkusta puolipisteeseen: `.separator ';'`
- Sen jälkeen voidaan importilla tuoda csv uuteen tauluun `.import 2020_tiliote.csv tiliotetaulu`
- Tarkastetaan taulun skeema (eli saraketiedot) `.schema tiliotetaulu`

Alla esimerkki:

```sql
sqlite> .mode csv
sqlite> .separator ';'
sqlite> .import 2020_tiliote.csv tiliotetaulu
sqlite> .schema tiliotetaulu
CREATE TABLE tiliotetaulu(
  "Kirjauspäivä" TEXT,
  "Arvopäivä" TEXT,
  "Määrä EUROA" TEXT,
  "Laji" TEXT,
  "Selitys" TEXT,
  "Saaja/Maksaja" TEXT,
  "Saajan tilinumero ja pankin BIC" TEXT,
  "Viite" TEXT,
  "Viesti" TEXT,
  "Arkistointitunnus" TEXT
);
sqlite>
```

Palautetaan vielä mode järkevämpään muotoon

```sql
sqlite> .mode tabs
sqlite> .header on
```

Jolloin sarakkeiden sisällöt näkyy kokonaan.

Nyt tietokantasi ja uusi taulusi ovat valmiita SQL kyselyitä varten.

sqlite3 sovelluksesta poistut komennolla `.exit` (aina piste ekana kun
kyseessä on sqlite komento, eikä sql kielen komento)

## Taustaa sql kielestä

Tässä välissä voisi syytä olla kerrata SQL kieltä hieman.

## Harjoitukset

### Harj.1 viitteet

#### Harj.1 ohje

Poimi kaikki uniikit eli yksilölliset viitteet omaan sarakkeeseen ja
lisää vierelle sarake jossa summattuna rivien Euromäärät per viite.

Poimi vain rivit joilla Viitteen pituus on suurempi kuin 0.

Järjestele poiminta summan mukaan suurimmasta pienimpään.

Havainne:

```
Viite|Summa
12345|3000
23456|2031
77234|653
```

#### Harj.1 vastaus

Kyselyitä voi tehdä suoraan sqlite sovelluksesta tai sql -tiedostojen
avulla kutsumalla sqlite3:a komentoriviltä.

Luodaan uusi tiedosto `viite.sql`

Jossa sisältönä:

```sql
select
  distinct Viite,
  sum("Määrä EUROA") as summa
from
  tiliotetaulu
where
  length(Viite) > 0
group by
  Viite
order by
  summa desc;
```

Ja suoritetaan kysely sqlite3:lle täten:

```bash
.\sqlite3.exe tietokanta.db ".header on" ".mode tabs" ".read viite.sql"
```

Ensin kutsutaan sqlite3 sovellusta, jolle aina annetaan ensimmäisenä arvona tietokannan tiedoston
nimi. Tämän perään voidaan hipsujen väliin kirjoittaa komentoja jota sqlite tulkitsee riveiksi.

